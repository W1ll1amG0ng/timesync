# Time Sync
雷达和相机检测框的时间同步工具
# 依赖
+ std_msgs
+ fsd_common_msgs
+ roscpp
+ rospy

# 构建
1. 将该包放置于含有`fsd_common_msgs`的工作空间下
1. `catkin build timesync`

# 使用
## 单独使用

    roslaunch timesync run.launch

## 作为库使用
请使用头文件`timesync/timesync.h`

# 时间同步过程
1. 缓存雷达和相机的话题，以雷达时间戳为基准，寻找比这个时间新的最近的一帧和比这个时间早的最近一帧相机话题
1. 对两帧相机话题数据进行线性插值