#include "timesynchandle.h"

int main(int argc, char** argv) {

    ros::init(argc, argv, "timesync");

    TimeSyncHandle handle;
    handle.start();
    ros::Rate r(10);

    while(ros::ok()) {
        ros::spinOnce();
        handle.sync();
        r.sleep();
    }
    return 0;
}