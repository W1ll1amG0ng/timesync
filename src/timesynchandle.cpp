#include "timesynchandle.h"

#include <iostream>

TimeSyncHandle::TimeSyncHandle() : node_("~") {

    if (!node_.param<std::string>("timesync/camera_topic", camera_topic_name_, "/perception/camera/cone_detections")) {
        ROS_WARN_STREAM("Did not load timesync/camera_topic. Standard value is: " << camera_topic_name_);
    }
    if (!node_.param<std::string>("timesync/lidar_topic", lidar_topic_name_, "/perception/lidar/cone_detections")) {
        ROS_WARN_STREAM("Did not load timesync/lidar_topic. Standard value is: " << lidar_topic_name_);
    }
    if (!node_.param<std::string>("timesync/output_camera_topic", output_camera_topic_name_, "/perception_syn/camera/cone_detections")) {
        ROS_WARN_STREAM("Did not load timesync/output_camera_topic. Standard value is: " << output_camera_topic_name_);
    }
    if (!node_.param<std::string>("timesync/output_lidar_topic", output_lidar_topic_name_, "/perception_syn/lidar/cone_detections")) {
        ROS_WARN_STREAM("Did not load timesync/output_lidar_topic. Standard value is: " << output_lidar_topic_name_);
    }

    if (!node_.param("timesync/lidar_buffer_size", lidar_buffer_size_, 10)) {
        ROS_WARN_STREAM("Did not load timesync/lidar_buffer_size. Standard value is: " << lidar_buffer_size_);
    }
    if (!node_.param("timesync/camera_buffer_size", camera_buffer_size_, 30)) {
        ROS_WARN_STREAM("Did not load timesync/camera_buffer_size. Standard value is: " << camera_buffer_size_);
    }
    if (!node_.param("timesync/max_search_count", max_search_count_, 10)) {
        ROS_WARN_STREAM("Did not load timesync/max_search_count. Standard value is: " << max_search_count_);
    }

    time_sync_ = std::shared_ptr<TimeSync>((new TimeSync(lidar_buffer_size_, camera_buffer_size_, max_search_count_)));

}



void TimeSyncHandle::start() {

    lidar_sub_ = node_.subscribe<fsd_common_msgs::ConeDetections>(lidar_topic_name_, 1, &TimeSyncHandle::lidarCallback, this);
    camera_sub_ = node_.subscribe<fsd_common_msgs::YoloConeDetections>(camera_topic_name_, 1, &TimeSyncHandle::cameraCallback, this);
    syn_lidar_pub_ = node_.advertise<fsd_common_msgs::ConeDetections>(output_lidar_topic_name_, 1, true);
    syn_camera_pub_ = node_.advertise<fsd_common_msgs::YoloConeDetections>(output_camera_topic_name_, 1, true);

    std::cout << "Start! " << std::endl;
}



void TimeSyncHandle::sync() {
    SyncData data;
    int res = time_sync_->sync(data);
    switch (res) {

        case 0:
            syn_camera_pub_.publish(data.camera);
            //ROS_INFO("publish lidar size: %d", data.lidar.cone_detections.size());
            syn_lidar_pub_.publish(data.lidar);
            ROS_INFO("Data published. ");
            break;
        case 1:
            ROS_WARN("Lidar buffer is empty");
            break;
        case 2:
            ROS_WARN("Camera buffer is empty");
            break;
        case 3:
            ROS_WARN("No old camera data");
            break;
        case 4:
            ROS_INFO("In searching");
            break;
        case 5:
            ROS_WARN("Searching failed, pass. ");
            break;
    
    }
}



void TimeSyncHandle::lidarCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg) {

    /*
    fsd_common_msgs::ConeDetections temp;
    temp.header = msg->header;
    temp.cone_detections = msg->cone_detections;

    time_sync_->inputLidar(temp);
    */
   time_sync_->inputLidar(*msg);
}



void TimeSyncHandle::cameraCallback(const fsd_common_msgs::YoloConeDetections::ConstPtr& msg) {

    /*
    fsd_common_msgs::YoloConeDetections temp;
    temp.header = msg->header;
    temp.cone_detections = msg->cone_detections;
    temp.image_header = msg->image_header;

    time_sync_->inputCamera(temp);
    */
   time_sync_->inputCamera(*msg);

}