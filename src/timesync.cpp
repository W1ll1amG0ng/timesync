#include "timesync/timesync.h"
#include "timesync/camerainterpolation.h"

#include <iostream>
#include <vector>

TimeSync::TimeSync(int lidar_buffer_size, int camera_buffer_size, int max_search_count) {

    lidar_buffer_size_ = lidar_buffer_size;
    camera_buffer_size_ = camera_buffer_size;
    max_search_count_ = max_search_count;

}



void TimeSync::inputLidar(fsd_common_msgs::ConeDetections data) {

    if(lidar_data_count_ < lidar_buffer_size_) {
        lidar_buffer_.push(data);
        lidar_data_count_++;
    }

}



void TimeSync::inputCamera(fsd_common_msgs::YoloConeDetections data) {

    if(camera_data_count_ < camera_buffer_size_) {
        camera_buffer_.push(data);
        camera_data_count_++;
    }

}



int TimeSync::sync(SyncData &res) {

    /**
     * return code: 
     * 0 - normal
     * 1 - lidar buffer is empty
     * 2 - camera buffer is empty
     * 3 - no old camera data
     * 4 - in searching
     * 5 - search failed, pass
     */
    


    // check lidar
    if(!is_searching_) {
        if(lidar_buffer_.empty()) return 1;
    }
    // check camera
    if(camera_buffer_.empty()) return 2;

    fsd_common_msgs::ConeDetections lidar_data;
    ros::Time lidar_time;
    if(!is_searching_) {

        //init selected camera and flags
        std::vector<fsd_common_msgs::YoloConeDetections>().swap(selected_camera_);
        search_count = 0;

        // extract lidar
        lidar_data = lidar_buffer_.front();
        lidar_buffer_.pop();
        lidar_data_count_--;

        lidar_time = lidar_data.header.stamp;
        lidar_searching_ = lidar_data;
        std::cout << "lidar time: " << lidar_time.toSec() << std::endl;

    } else {
        lidar_data = lidar_searching_;
        lidar_time = lidar_data.header.stamp;
    }

    // search camera
    fsd_common_msgs::YoloConeDetections camera_data = camera_buffer_.front();
    ros::Time camera_time = camera_data.header.stamp;

    if(!is_searching_) {
        if(camera_time > lidar_time) return 3;
    }

    is_searching_ = true;

    selected_camera_.push_back(camera_data);
    camera_buffer_.pop();
    camera_data_count_--;
    search_count++;

    if(is_searching_) {

        if(camera_time >= lidar_time) {
            is_searching_ = false;
        } else {
            return 4;
        }

        if(search_count >= max_search_count_) {
            is_searching_ = false;
            return 5;
        }

    }



    /*
    bool end_flag = false;
    while(!end_flag) {
        if(!camera_buffer_.empty()) {

            camera_data = camera_buffer_.front();
            camera_time = camera_data.header.stamp;

            if(camera_time >= lidar_time) end_flag = true;

            selected_camera.push_back(camera_data);
            camera_buffer_.pop();
            camera_data_count_--;

        } else {
            continue;
        }
    }
    */

    std::cout << "Selected camera size: " << selected_camera_.size() << std::endl;

    // select 2 camera
    std::vector<fsd_common_msgs::YoloConeDetections>::reverse_iterator iter = selected_camera_.rbegin();
    fsd_common_msgs::YoloConeDetections camera_after = *iter;
    iter++;
    fsd_common_msgs::YoloConeDetections camera_before = *iter;

    std::cout << "before: " << camera_before.header.stamp.toNSec() << " "
              << "lidar: " << lidar_time.toNSec() << " "
              << "after: " << camera_after.header.stamp.toNSec() << std::endl;

    if(camera_after.header.stamp == lidar_time) {
        res.camera = camera_after;
        res.lidar = lidar_data;
        return 0;
    }
    if(camera_before.header.stamp == lidar_time) {
        res.camera = camera_before;
        res.lidar = lidar_data;
        return 0;
    }

    // Interpolation
    CameraInterpolation polation(camera_before, camera_after);
    std::cout << "polation init done" << std::endl;
    fsd_common_msgs::YoloConeDetections new_camera = polation.calc(lidar_time);
    res.camera = new_camera;
    res.lidar = lidar_data;

    // end
    return 0;

}