#include "timesync/camerainterpolation.h"

#include <cmath>
#include <limits>
#include <vector>

fsd_common_msgs::YoloConeDetections CameraInterpolation::calc(ros::Time lidar_time) {

    //check
    ros::Time camera1_time = camera1_.header.stamp;
    ros::Time camera2_time = camera2_.header.stamp;
    if(camera1_time == camera2_time) return camera1_;
    if(camera1_time > camera2_time) {
        fsd_common_msgs::YoloConeDetections temp = camera1_;
        camera1_ = camera2_;
        camera2_ = temp;
    }

    //init
    std::vector<YoloConePair> pairs;
    fsd_common_msgs::YoloConeDetections res = camera1_;
    res.header.stamp = lidar_time;
    std::vector<fsd_common_msgs::YoloCone>().swap(res.cone_detections);

    //registration using ICP
    for(auto i = camera1_.cone_detections.begin(); i != camera1_.cone_detections.end(); i++) {

        YoloConePair pair;
        pair.cone_before = *i;

        auto min_iter = camera2_.cone_detections.begin();
        double min_dis = std::numeric_limits<double>::max();
        for(auto j = camera2_.cone_detections.begin(); j != camera2_.cone_detections.end();j++) {

            double i_j_dis = dis(*i, *j);
            if(min_dis > i_j_dis) {
                min_dis = i_j_dis;
                min_iter = j;
            }

        }

        if(min_dis == std::numeric_limits<double>::max()) continue; // drop if registration failed

        pair.cone_after = *min_iter;
        //camera2_.cone_detections.erase(min_iter);

        pairs.push_back(pair);

    }

    // Interpolation
    for(auto i = pairs.begin(); i != pairs.end(); i++) {

        fsd_common_msgs::YoloCone new_cone = i->cone_before;

        LinearInterpolation polation;
        polation.setX(camera1_time.toNSec(), camera2_time.toNSec());
        polation.setY(i->cone_before.x.data, i->cone_before.y.data, i->cone_after.x.data, i->cone_after.y.data);
        vector2 new_pos = polation.calc(lidar_time.toNSec());
        new_cone.x.data = new_pos.x;
        new_cone.y.data = new_pos.y;

        res.cone_detections.push_back(new_cone);

    }

    return res;

}




double CameraInterpolation::dis(const fsd_common_msgs::YoloCone &a, const fsd_common_msgs::YoloCone &b) {
    double a_x = a.x.data;
    double a_y = a.y.data;
    double b_x = b.x.data;
    double b_y = b.y.data;

    return sqrt((a_x-b_x)*(a_x-b_x) + (a_y-b_y)*(a_y-b_y));
}