#include "timesync/linearinterpolation.h"

vector2 LinearInterpolation::calc(double x) {

    if(x1_ == x2_) return y1_;
    double delta_x1_x2 = x2_ - x1_;
    double delta_x1_x = x - x1_;
    if(delta_x1_x < 0) return {0, 0};

    vector2 res;

    //calc y_x
    double k_y_x = (y2_.x - y1_.x) / delta_x1_x2;
    res.x = k_y_x * delta_x1_x + y1_.x;

    //calc y_y
    double k_y_y = (y2_.y - y1_.y) / delta_x1_x2;
    res.y = k_y_y * delta_x1_x + y1_.y;

    return res;

}