#pragma once

#include "fsd_common_msgs/ConeDetections.h"
#include "fsd_common_msgs/YoloConeDetections.h"

#include <ros/time.h>

#include <queue>

struct SyncData {
    fsd_common_msgs::ConeDetections lidar;
    fsd_common_msgs::YoloConeDetections camera;
};

class TimeSync {

    public:
        TimeSync(int lidar_buffer_size, int camera_buffer_size, int max_search_count);

        void inputLidar(fsd_common_msgs::ConeDetections data);
        void inputCamera(fsd_common_msgs::YoloConeDetections data);
        int sync(SyncData &res);

    private:
        std::queue<fsd_common_msgs::ConeDetections> lidar_buffer_;
        std::queue<fsd_common_msgs::YoloConeDetections> camera_buffer_;

        int lidar_data_count_ = 0;
        int camera_data_count_ = 0;

        bool is_searching_ = false;
        int search_count = 0;
        fsd_common_msgs::ConeDetections lidar_searching_;
        std::vector<fsd_common_msgs::YoloConeDetections> selected_camera_;

        // parameters
        int lidar_buffer_size_;
        int camera_buffer_size_;
        int max_search_count_;

};