#pragma once

#include "timesync/linearinterpolation.h"
#include "fsd_common_msgs/YoloConeDetections.h"

struct YoloConePair {
    fsd_common_msgs::YoloCone cone_before;
    fsd_common_msgs::YoloCone cone_after;
};

class CameraInterpolation {

    public:
    CameraInterpolation(fsd_common_msgs::YoloConeDetections camera1, fsd_common_msgs::YoloConeDetections camera2) {camera1_ = camera1; camera2_ = camera2;}
    fsd_common_msgs::YoloConeDetections calc(ros::Time lidar_time);

    private:
        fsd_common_msgs::YoloConeDetections camera1_, camera2_;

        // private methods
        double dis(const fsd_common_msgs::YoloCone &a, const fsd_common_msgs::YoloCone &b);

};