#pragma once

struct vector2 {
    double x;
    double y;
};

class LinearInterpolation {

    public:
    LinearInterpolation() {}
    void setX(double x1, double x2) {x1_ = x1; x2_ = x2;}
    void setY(vector2 y1, vector2 y2) {y1_ = y1; y2_ = y2;}
    void setY(double y1_x, double y1_y, double y2_x, double y2_y) {
        y1_.x = y1_x;
        y1_.y = y1_y;
        y2_.x = y2_x;
        y2_.y = y2_y;
    }

    vector2 calc(double x);

    private:
        double x1_, x2_;
        vector2 y1_, y2_;

};