#pragma  once

#include "timesync/timesync.h"

#include "fsd_common_msgs/ConeDetections.h"
#include "fsd_common_msgs/YoloConeDetections.h"

#include <ros/ros.h>

#include <string>
#include <memory>

class TimeSyncHandle {

    public:
        TimeSyncHandle();
        void start();
        void sync();

        // Callbacks
        void lidarCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg);
        void cameraCallback(const fsd_common_msgs::YoloConeDetections::ConstPtr& msg);

    private:
        ros::NodeHandle node_;
        std::shared_ptr<TimeSync> time_sync_;

        // pub & sub
        ros::Subscriber lidar_sub_;
        ros::Subscriber camera_sub_;
        ros::Publisher syn_lidar_pub_;
        ros::Publisher syn_camera_pub_;

        // parameters
        std::string lidar_topic_name_;
        std::string camera_topic_name_;
        std::string output_lidar_topic_name_;
        std::string output_camera_topic_name_;
        int lidar_buffer_size_;
        int camera_buffer_size_;
        int max_search_count_;

};